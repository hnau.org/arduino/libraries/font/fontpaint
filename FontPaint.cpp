#include "FontPaint.h"

FontPaint::FontPaint() {
	font = nullptr;
	foreground = RGBAs::black;
	background = RGBAs::transparent;
	glyphsSeparation = 1;
}

void FontPaint::updateCellWidth() {
	if (font == nullptr) {
		return;
	}
	cellWidth = font->getGlyphWidth() + glyphsSeparation;
}

void FontPaint::drawChar(char ch, Canvas& canvas, Font& font, size_t x, size_t y) {
	for (size_t ix = 0; ix < font.getGlyphWidth(); ix++) {
		for (size_t iy = 0; iy < font.getGlyphHeight(); iy++) {
			bool pixel = font.getPixel(ch, ix, iy);
			RGBA color = pixel ? foreground : background;
			canvas.setPixelColor(x+ix, y+iy, color);
		}
	}
}

void FontPaint::setBackground(RGBA background) {
	this->background = background;
}

void FontPaint::setForeground(RGBA foreground) {
	this->foreground = foreground;
}

void FontPaint::setFont(Font& font) {
	this->font = &font;
	updateCellWidth();	
}

void FontPaint::setGlyphsSeparation(size_t glyphsSeparation) {
	this->glyphsSeparation = glyphsSeparation;
	updateCellWidth();
}
	
void FontPaint::drawChar(char ch, Canvas& canvas, size_t x, size_t y) {
	if (font == nullptr) {
		return;
	}
	drawChar(ch, canvas, *font, x, y);
}

void FontPaint::drawChars(CharsProvider chars, Canvas& canvas, size_t x, size_t y) {
	if (this->font == nullptr) {
		return;
	}
	Font& font = *(this->font);
	chars(
		[&](char ch) {
			if (font.checkGlyphIsAvailable(ch)) {
				drawChar(ch, canvas, font, x, y);
				x += cellWidth;
			}
		}
	);
}
