#ifndef FONTPAINT_H
#define FONTPAINT_H

#include <RGBA.h>
#include <Font.h>
#include <Canvas.h>
#include <CharsProvider.h>

class FontPaint {
	
private:

	Font* font;
	RGBA foreground;
	RGBA background;
	
	size_t cellWidth;
	size_t glyphsSeparation;
	
	void updateCellWidth();
	void drawChar(char ch, Canvas& canvas, Font& font, size_t x, size_t y);
	

public:

	FontPaint();
	
	void setBackground(RGBA background);
	void setForeground(RGBA foreground);
	void setFont(Font& font);
	void setGlyphsSeparation(size_t glyphsSeparation);
	
	void drawChar(char ch, Canvas& canvas, size_t x, size_t y);
	void drawChars(CharsProvider chars, Canvas& canvas, size_t x, size_t y);

};


#endif //FONTPAINT_H
